import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import Hotel from '../views/Hotel.vue'
import Room from '../views/Room.vue'
import Package from '../views/Package.vue'
import Services from '../views/Services.vue'
import FAQ from '../views/FAQ.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/hotel',
    name: 'hotel',
    component: Hotel
  },
  {
    path: '/packages',
    name: 'packages',
    component: Package
  },
  {
    path: '/services',
    name: 'services',
    component: Services
  },
  {
    path: '/rooms',
    name: 'rooms',
    component: Room
  },
  {
    path: '/faq',
    name: 'faq',
    component: FAQ
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior (to) {
    if (to.hash) {
      return {
        top: 10000
      }
    }

    return {
      top: 0
    }
  }
})
export default router

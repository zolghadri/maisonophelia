import { createStore } from 'vuex'

import data from '@/i18n/en.json'

export default createStore({
  state: {
    lang: 'EN',
    dictionary: data
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})

import { createI18n } from 'vue-i18n'

const messages = {
  en: require('@/i18n/en.json'),
  fr: require('@/i18n/fr.json')
}
export default createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  messages
})

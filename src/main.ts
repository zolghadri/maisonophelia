/* eslint-disable */
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap/dist/css/bootstrap.min.css'
import i18n from './plugins/i18n'
import scroll from '@/extentions/scroll.js'

const app = createApp(App)
app.use(store)
app.use(router)
app.use(i18n)

app.use(scroll, {
  class: 'v-scroll-reveal',
  duration: 800,
  scale: 1,
  distance: '10px',
  mobile: true
})
app.mount('#app')
